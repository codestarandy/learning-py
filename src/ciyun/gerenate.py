from wordcloud import WordCloud
import PIL.Image as image
import numpy as np

with open("files/tech.txt") as fp:
    text = fp.read()
    # print(text)
    mask = np.array(image.open("photos/tech.png"))
    word_cloud = WordCloud(
        mask=mask, repeat=True
    ).generate(text)
    image_produce = word_cloud.to_image()
    image_produce.show()
