"""
练习类的存取
"""

# %%
import pickle


class Bird(object):
    has_feather = True
    reproduction_method = "egg"


bird = Bird()

with open('files/bird.pkl', 'wb') as f:
    pickle.dump(bird, f)


# %%

with open("files/bird.pkl", 'rb') as f:
    bird2 = pickle.load(f)
print(bird2.has_feather)
print(bird2.reproduction_method)
