"""
这是一个文件操作的案例
"""
# %%
# 获取当前工作目录
import os
print(os.getcwd())
# 转到想要的工作目录
os.chdir('src/week8-Sunday/')
print(os.getcwd())
# **************************************************************
# %%
# t表示文本文件方式
with open("files/text.txt", "rt", encoding='utf-8') as f:
    print(f.readline())

# %%
with open("files/text.txt", 'rb') as bf:
    print(bf.readline())
    print(bf.readline().strip())


# %%
ls = ['唐诗\n', '宋词\n', '元曲']
with open('files/text.txt', 'w', encoding='utf-8') as f:
    f.writelines(ls)

# %%
with open('files/text2.txt', 'w', encoding='utf-8') as f:
    f.write("我们都渴望返校上课!!!")

# %%
word_freq = {}
with open('files/aa.txt', 'r', encoding='utf-8') as f:
    for line in f:
        words = line.strip().replace(',', ' ').replace('.',' ').split()
        for word in words:
            word_freq[word] = word_freq.get(word, 0) + 1
    freq_word = []
    for word, freq in word_freq.items():
        freq_word.append((word, freq))
    freq_word.sort(key=lambda x: -x[1])
    for i in range(10):
        print(freq_word[i])

