import json


def process_json_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as f:
        city_list = json.load(f)
    # 返回一个列表，列表元素是字典
    return city_list


def main():
    # filepath = input('请输入json文件名称：')
    filepath = 'beijing_aqi'
    filepath = 'files/' + filepath + '.json'
    city_list = process_json_file(filepath)
    print(city_list)
    city_list.sort(key=lambda city: city['aqi'])
    top5_list = city_list[:5]

    with open('files/top5_aqi.json', mode='w', encoding='utf-8') as f:
        json.dump(top5_list, f, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    main()
