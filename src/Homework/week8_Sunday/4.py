"""
功能:
将成绩表的文本文件转换并写入JSON文件
将成绩表的JSON文件转换并写入CSV文件
"""
import json
import csv


def txt2json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = f.readlines()
    print(data)
    for i in range(len(data)):
        data[i] = json.loads(data[i].replace('\'', '\"'))
    print(data)
    with open("files/grade1.json", 'w', encoding='utf-8') as f:
        for i in data:
            json.dump(i, f, sort_keys=False, indent=4, ensure_ascii=False)


def json2txt(filename):
    with open(filename, encoding='utf-8') as f:
        my_json = json.load(f)
    print(my_json)
    with open(filename[:-5] + '_new.csv', 'w', encoding='utf-8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['学号', '语文', '数学', '英语'])
        # tuple改为List也行，
        writer.writerows([[tuple(k.split(':'))[1], *tuple(v.values())] for k, v in my_json.items()])


if __name__ == '__main__':
    txt2json('files/grade1.txt')
    json2txt("files/grade1.json")