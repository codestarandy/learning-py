"""
功能:在其中添加总分关键字及总分,按总分由高到低列出学号和总分,
将成绩表写入文本文件（直接将字典的键值对作为一行）
"""
import random


def dic_generate(num):
    dic = {'学号:2020' + '{:0>4s}'.format(str(i)): {
        '语文': random.randint(0, 150),
        '数学': random.randint(0, 150),
        '英语': random.randint(0,150)
    }
        for i in range(num)
    }
    return dic


def main():
    num = eval(input("请您输入学生的个数: "))
    dic = dic_generate(num)
    for key, value in dic.items():
        value['总分'] = value['语文'] + value['数学'] + value['英语']