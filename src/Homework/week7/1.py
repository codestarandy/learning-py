def dec(func):
    def new_func(x, y):
        print('我是新加的功能')
        func(x, y)

    return new_func


@dec
def add_two(x, y):
    print(x + y)


if __name__ == '__main__':
    add_two(1, 2)
