class A:
    pass


# 这是一个类方法
@classmethod
def ask(cls):
    print('来自新加类方法的询问')


if __name__ == '__main__':
    # 新加类方法
    A.ask = ask
    A.ask()
