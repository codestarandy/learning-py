import types


class A:
    pass


def reply(self):
    print('你已经动态添加了我的可爱')


if __name__ == '__main__':
    a = A()
    # 动态添加实例方法
    a.reply = types.MethodType(reply, a)
    a.reply()
