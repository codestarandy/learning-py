import numpy as np
"""
# 数据格式: 
# 编号，学号，专业，课程，分数，学分
# Id, No, Major, Course, Score, Credit

# Sample 1(按学生平均成绩排序)
# No, Avg, Total, Excellent, Good, Medium, Pass, Fail 
# 学号，平均分，总课程数，优秀率，良好率，中等率，及格率，不及格率

# Sample 2（统计该专业各个平均分人数占比）
# Major, Total, Excellent, Good, Medium, Pass, Fail 
# 专业，专业总人数，优秀率，良好率，中等率，及格率，不及格率

# Sample 3（统计课程各个平均分人数占比）
# Course, Avg, Total, Excellent, Good, Medium, Pass, Fail 
# 课程，课程平均分，课程人数，优秀率，良好率，中等率，及格率，不及格率
"""


def read_data(file):
    """
    功能: 读取文件中的数据
    :param file: 要读取的文件路径
    :return: 学生的数据(二位列表), 列的属性
    """
    data = []
    with open(file) as f:
        head_property = f.readline().rstrip('\n').split(',')
        for row in f.readlines():
            col = row.rstrip('\n').split(',')
            data.append([int(col[0]), int(col[1]), int(col[2]), int(col[3]), float(col[4]), float(col[5])])
    return data, head_property


def split_data(key_to_split, data, head):
    """
    功能: 划分数据,进行groupby(利用字典)
    :param key_to_split:用到的groupby的关键属性
    :param data:二维数组
    :param head:属性
    :return:字典
    """
    # 获取划分属性的下标
    col = head.index(key_to_split)
    ret = {}
    for row in data:
        ret[row[col]] = ret.get(row[col], [])
        ret[row[col]].append(row)
    return dict((k, np.array(v)) for k, v in ret.items())


def show(key_to_split, data, head):
    """
    功能: 计算并输出一系列指标
    :param key_to_split:划分的关键属性
    :param data:数据(二维数组)
    :param head:属性
    :return:None
    """
    dic = split_data(key_to_split, data, head)
    res = []
    for key in dic:
        # 第5个位成绩,所以dic[key][:, 4]
        scores: np.ndarray = dic[key][:, 4]
        total = scores.shape[0]
        avg = f'{np.sum(scores, axis=0) / total:.2f}'
        excellent = f'{np.sum(scores >= 90) / total * 100:.2f}%'
        good = f'{np.sum((80 <= scores) * (scores < 90)) / total * 100:.2f}%'
        medium = f'{np.sum((70 <= scores) * (scores < 80)) / total * 100:.2f}%'
        pass_ = f'{np.sum((60 <= scores) * (scores < 70)) / total * 100:.2f}%'
        fail = f'{np.sum(scores < 60) / total * 100:.2f}%'
        res.append((key, avg, total, excellent, good, medium, pass_, fail))
    res.sort(key=lambda x: -float(x[1]))
    key_word = [key_to_split, 'Avg', 'Total', 'Excellent', 'Good', 'Medium', 'Pass', 'Fail']
    print(*[each.rjust(12) for each in key_word])
    for row in res:
        print(*[f'{col:>12}' for col in row])
    print('\n\n')


if __name__ == '__main__':
    dt, hp = read_data('data/cs.csv')
    # show('No', dt, hp)
    # show('Major', dt, hp)
    show('Course', dt, hp)

