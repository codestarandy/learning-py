"""
尝试reduce函数
"""
from functools import reduce


def add(x, y):
    return x + y


if __name__ == '__main__':
    x1 = reduce(add, [1, 2, 3])
    print('x1: ', x1)
    x2 = reduce(lambda x, y: x + y, [1, 2, 3, 4, 5, 6])
    print('x2: ', x2)