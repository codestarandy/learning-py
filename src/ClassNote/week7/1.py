"""
作用: 试水map函数
map(function, 要输入函数的参数，可以是列表等)
"""


def square(x):
    return x ** 2


if __name__ == '__main__':
    # print(list(map(square, [1, 2, 3, 4])))
    x1 = list(map(lambda x: x ** 2, [1, 2, 3, 4]))
    print('x1: ', x1)
    x2 = list(map(lambda x, y: x + y, [1, 2, 3], [4, 5, 6]))
    print('x2: ', x2)
