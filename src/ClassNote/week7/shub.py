# !/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2020/5/1 12:43
# @Author : pan64271
# @File   : english_names_normalizer.py

if __name__ == '__main__':
    L1 = ['kIttY', 'Tom', 'richard']
    L2 = list(map(lambda x: x.capitalize(), L1))
    print(L2)





# !/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2020/5/1 12:49
# @Author : pan64271
# @File   : str2float_by_map_and_reduce.py

from functools import reduce


def str2float(s):
    """Transform a string to a float.

    Only deal with the string with the format 'xxx.yyy'.
    Only digits and dot are allowed. No signs and other characters.
    Do not check the validity of parameter.

    :param s: the string to be transformed
    :type s: string
    :return: the float after transformation
    :rtype: float
    """
    # find the index of the decimal dot
    index_of_dot = s.index('.') if '.' in s else len(s) - 1
    # generate the integer version of integer part and reversed decimal part
    integer_part = map(lambda x: ord(x) - 0x30, s[0: index_of_dot])
    decimal_part = map(lambda x: ord(x) - 0x30, s[len(s) - 1: index_of_dot: -1])
    # combine the two parts
    return reduce(lambda x, y: x * 10.0 + y, integer_part, 0.0) + reduce(lambda x, y: x / 10.0 + y, decimal_part, 0.0) / 10.0


if __name__ == '__main__':
    print('str2float(\'123.456\') = ', str2float('123.456'))
    if abs(str2float('123.456') - 123.456) < 0.00001:
        print('Test succeeded!')
    else:
        print('Test failed!')