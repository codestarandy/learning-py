# example10_16.py
# coding=utf-8
'''
读取stock.xlsx文件中的股票信息，选取收盘价大于13的数据行、开盘价最高的数据行、开盘价
位于[12.5, 13.5]区间内的数据行、开盘价或收盘价为空的数据行、随机选取3行数据。
'''
import numpy as np
import pandas as pd

df = pd.read_excel('stock.xlsx', sheet_name='stock', usecols='A:E,G')
print(df)
print('开盘价大于13的数据行：\n', df[df['Open'] > 13])
print('开盘价最高的数据行：\n', df[df.Open == max(df['Open'])])
print('收盘价位于[12.5,13.5]区间内的数据行：\n',
      df[df.Close.between(12.5, 13.5)])
print('开盘价或收盘价为空值的数据行：\n',
      df[df.Open.isnull() | df.Close.isnull()])

# 使用两种方法随机采样
# 1.生成[0, len(df))区间内随机的3个整数
i = 3
r = np.random.randint(0, len(df), i)
print('随机产生的行号为：', r)
print('随机选取的%d行数据为：\n' % i, df.loc[r, :])
# 2.使用sample函数
print(df.sample(3, replace=False))
