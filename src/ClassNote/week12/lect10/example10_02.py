"""
DataFrame 的创建
"""

import numpy as np
import pandas as pd

# %%
# 通过numpy数组创建
n = np.random.randint(low=0, high=10, size=(5, 5))
print(n)

# %%
df1 = pd.DataFrame(data=n)
print(df1)

# %%
df2 = pd.DataFrame(data=n, columns=list('ABCDE'))
print(df2)

# %%
# 由字典创建
data = {'province': ['广东', '山东', '河南', '四川', '江苏', '河北', '湖南', '安徽', '湖北', '浙江'],
        'population': [10999, 9946, 9532, 8262, 7998, 7470, 6822, 6195, 5885, 5590],
        'city': ['广州', '济南', '郑州', '成都', '南京', '石家庄', '长沙', '合肥', '武汉', '杭州']}

cc = pd.DataFrame(data)
print(cc)

# %%
print(cc['city'])

# %%
print(cc.index[6])

# %%
print(cc['city'][6])

# %%
cc = pd.DataFrame(data, columns=['province', 'population', 'city'])
print(cc)

# %%
cc = pd.DataFrame(data, columns=['province', 'population', 'city', 'area'])
print(cc)

# %%
# 通过series创建dataframe
s8 = pd.Series(np.arange(5), index=list('abcde'))
s9 = pd.Series(range(5, 10), index=list('abcde'))
s10 = pd.Series([10, 11, 12, 13, 14], index=list('abcdf'))

df3 = pd.DataFrame([s8, s9, s10])

print(df3)
