# example10_26.py

# 删除Series的一个元素:

import pandas as pd
import numpy as np

ser = pd.Series([4.5, 7.2, -5.3, 3.6], index=['d', 'b', 'a', 'c'])
ser.drop('c')

print(ser.drop('c'))

# 删除DataFrame的一行或列:
df = pd.DataFrame(np.arange(9).reshape(3, 3), index=['a', 'c', 'd'], columns=['oh', 'te', 'ca'])
print(df)
df.drop('a')
df.drop(['oh', 'te'], axis=1)
