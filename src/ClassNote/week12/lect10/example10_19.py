# example10_19.py
# coding=utf-8
"""
读取stock.xlsx文件中的股票信息作为一个DataFrame，计算每天最高价与最低价的
算术平均值。将此平均值作为单独的一列添加到原始数据的DataFrame对象中。
"""
import pandas as pd

df = pd.read_excel('stock.xlsx', sheet_name='stock',
                   index_col='Date', usecols='A:E', nrows=5)
print('原始DataFrame对象：\n', df)
# 计算每天最高价与最低价的算术平均值
result = (df.High + df.Low) / 2
df['mean'] = result
print('添加平均价格后的DataFrame：\n', df)
