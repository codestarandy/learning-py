"""
    案例：年度销售数据分析V2.0
    功能：1.提取2019年度的商品销售数据；
         2.处理与业务流程不符合数据，具体包括：
         (1)支付时间早于下单时间，
         (2)支付时间间隔过长（大于30分钟），
         (3)订单金额与支付金额为负数的数据。
    新增功能：
         3.进一步清洗数据，逐列检查，修补空值，填补缺失值。

"""

import pandas as pd

# 读取数据文件
df = pd.read_excel('order2019.xlsx', index_col='id')
print(df)  # 104557行

# 1.提取2019年数据
# ... ...
# 2.提取数据时,处理与业务流程不符合数据
# ... ...
# 引入时间模块, 确定周期时间
import datetime

startTime = datetime.datetime(2019, 1, 1)
endTime = datetime.datetime(2019, 12, 31, 23, 59, 59)
# 将数据源中的时间数据转换成datetime形式 
df.orderTime = pd.to_datetime(df.orderTime)
df.payTime = pd.to_datetime(df.payTime)
# 将2019年1月1日前数据删除
print(df[df.orderTime < startTime])
df.drop(index=df[df.orderTime < startTime].index, inplace=True)
# 将2019年12月31日后数据删除 
print(df[df.orderTime > endTime])
df.drop(index=df[df.orderTime > endTime].index, inplace=True)
print(df)  # 104296行

# 2.提取数据时,处理与业务流程不符合数据

# 计算下单时间与支付时间间隔
df['payinterval'] = (df.payTime - df.orderTime).dt.total_seconds()
# (1) 支付时间间隔大于30分钟
print(df[df.payinterval > 1800])
df.drop(index=df[df.payinterval > 1800].index, inplace=True)
# (2) 支付时间早于下单时间
df.drop(index=df[df.payinterval < 0].index, inplace=True)
print(df)  # 103354行
# (3) 订单金额与支付金额为负
print(df[df.orderAmount < 0])
print(df[df.payment < 0])
df.drop(index=df[df.payment < 0].index, inplace=True)
print(df)  # 103348

# 查看数据
df.info()
df.describe()

# 3.清洗数据

# (1)清洗orderID
print(df.orderID.unique().size)  # 计算非重复项的长度
df.drop(index=df[df.orderID.duplicated()].index, inplace=True)  # 删除重复值的行
print(df.orderID.unique().size)  # 重新计算非重复项的长度
df.info()  # 103321

# (2)清洗userID
print(df.userID.unique().size)  # 计算非重复项的长度：78525
# df.info() #103321

# (3)清洗goodsID
print(df.goodsID[df.goodsID == 'PR000000'].size)
df.drop(index=df[df.goodsID == 'PR000000'].index, inplace=True)  # 删除空缺行
df.info()  # 103146

# (4)清洗chanelID
# 查看chanelID空值
print(df[df.chanelID.isnull()])
# 对空值进行修补
df['chanelID'].fillna(value=df.chanelID.mode()[0], inplace=True)
df.info()  # 103146

# (5) 清洗platformtype
print(df.platfromType.unique().size)  # 11
df.platfromType.unique()
df['platfromType'] = df['platfromType'].map(str.strip)  # 删除字符串的头和尾的空格,以及位于头尾的\n\t符号
df['platfromType'] = df['platfromType'].str.replace(" ", "")  # 删除字符串中出现的空格
df.platfromType.unique()

# (6) 清洗payment
# a.创建折扣字段
df['discount'] = (df.payment / df.orderAmount)
print(df.describe())
# b.平均折扣
meanDiscount = df[df['discount'] <= 1].discount.sum() / df[df['discount'] <= 1].discount.size
print(meanDiscount)
# c.找到折扣大于1的数据
print(df[df['discount'] > 1])
df['payment'] = df['payment'].mask(df['discount'] > 1, None)
# d.对折扣大于1的数据进行填补
df.info()
df['payment'].fillna(value=df.orderAmount * meanDiscount, inplace=True)
# e.处理折扣
df['discount'] = round((df.payment / df.orderAmount), 2)
df
df.info()  # 103146

# 查看数据
df.describe()
