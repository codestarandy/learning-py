"""
DateFrame.merge()数据连接
"""
# example10_27.py

import pandas as pd

dataDf1 = pd.DataFrame({'lkey': ['foo', 'bar', 'baz', 'foo'],
                        'value': [1, 2, 3, 4]})
dataDf2 = pd.DataFrame({'rkey': ['foo', 'bar', 'qux', 'bar'],
                        'value': [5, 6, 7, 8]})
print(dataDf1)
print(dataDf2)

# inner链接
dataLfDf = dataDf1.merge(dataDf2, left_on='lkey', right_on='rkey')

print(dataLfDf)
