# example15_22.py
# coding=utf-8
import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_excel('stock.xlsx', sheet_name='stock',
                   usecols='B,E')

df.plot(kind='box')  # kind='box'指定箱形图
plt.grid()
plt.title('开盘价与收盘价')
plt.show()
