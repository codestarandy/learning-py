# example10_17.py
# coding=utf-8
'''
读取stock.xlsx中的股票相关数据，分别实现按列名、索引和最高价列数据进行排序。
'''
import pandas as pd

df = pd.read_excel('stock.xlsx', sheet_name='stock',
                   index_col='Date', usecols='A:E', nrows=10)
print('排序前的DataFrame对象：\n', df)

# 以最高价来排序，默认为升序
# df2=df.sort_index(by=['High']),带by参数的排序.推荐使用sort_values()方法
df2 = df.sort_values(by=['High'])
print('以最高价升序排列：\n', df2)
# 原来的df保持不变
print('原DataFrame对象保持不变：\n', df)

# 按列名标签排序（默认升序）
df3 = df.sort_index(axis=1)
print('按列名升序排列后的DataFrame：\n', df3)

# 默认axis=0, 按索引排序; ascending=False降序
df4 = df2.sort_index(ascending=False)
print('按索引降序排列后的DataFrame：\n', df4)
