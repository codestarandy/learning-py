"""
Series 的创建
"""
# %%
import pandas as pd
import numpy as np

s0 = pd.Series(range(15))
print(s0)

# %%
# 自动索引，由0开始， 通过列表创建
s1 = pd.Series([1, 3, 5, np.nan, 6, 9])
print(s1)

# %%
# 自定义索引，通过可迭代对象创建
s2 = pd.Series(range(0, 4), index=list('ABCD'))
print(s2)

# %%
# 通过字典创建
d = dict(a=22, b=18, c=35)
s3 = pd.Series(d)
print(s3)

# %%
s4 = pd.Series({"a": 1, "b": 2, "c": 3}, index={'c', 'd', 'a', 'b'})
print(s4)

# %%
# 通过一维数组创建
s5 = pd.Series(np.arange(4), index=list('ABEF'))
print(s5)

# %%
# 通过标量(常数)创建
i = 2020
s6 = pd.Series(i)
print(s6)

# %%
# 创建一个含有相同元素的Series，元素的个数取决于我们设置的索引的个数
s7 = pd.Series(i, index=['a', 'b', 'c'])
print(s7)

# %%
# 修改series对象
s7.name = 'new_Series'
s7['a'] = 2018
s7[1] = 2019
print(s7)