# example10_23.py
"""
pandas.to_datetime()函数
"""
# 将不同的数据类型转化为统一的时间格式

import pandas as pd

# 1. str
data = '2020/05/15'
print(type(data))

timestamp = pd.to_datetime(data, format='%Y-%m-%d')
print(type(timestamp))
print(timestamp)

df = pd.read_excel('example.xlsx', index_col='id')

# 2. float
print(type(df.orderTime[1]))
timestamp1 = pd.to_datetime(df.orderTime[1], format='%Y-%m-%d')
print(type(timestamp1))
print(timestamp1)

# 3. datetime
print(type(df.orderTime[2]))
timestamp2 = pd.to_datetime(df.orderTime[2], format='%Y-%m-%d')
print(type(timestamp2))
print(timestamp2)
