# -*- coding: utf-8 -*-

import pandas as pd

obj = pd.Series(['c', 'a', 'd', 'a', 'a', 'b', 'b', 'c', 'c', 'c'])

print(obj)

print(obj.unique())

data = {'Q1': [1, 3, 4, 4, 4], 'Q2': [2, 3, 2, 2, 3], 'Q3': [1, 5, 2, 4, 4]}

frame = pd.DataFrame(data)

print(frame)

print(frame.Q1.unique())

print(frame.Q2.unique())

print(frame.Q3.unique())
