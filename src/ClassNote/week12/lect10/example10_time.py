"""
Pandas时间序列
"""

from datetime import datetime
from datetime import timedelta

now = datetime.now()

print(now)
print(now.year)
print(now.month)
print(now.day)

# timedelta表示两个datetime对象之间的时间差
delta = datetime(2020, 5, 7) - datetime(2019, 2, 24, 8, 15)
print(delta)  # datetime.timedelta(days=437, seconds=56700) timedelta表示两个datetime对象之间的时间差

# 可以给datetime对象加上（或减去）一个或多个timedelta，这样会产生一个新对象
start = datetime(2019, 2, 24)
print(start + timedelta(365))  # datetime.datetime(2020, 2, 24, 0, 0)
print(start - 2 * timedelta(365))  # datetime.datetime(2017, 2, 24, 0, 0)

# 字符串和datetime的相互转换
stamp = datetime(2020, 5, 9)
print(str(stamp))  # '2020-05-09 00:00:00'
print(stamp.strftime('%Y-%m-%d'))

# datetime.strptime可以将格式化编码将字符串转换为日期
value = '2020-05-09'
dt = datetime.strptime(value, '%Y-%m-%d')
print(dt)

date_str = ['7/6/2020', '8/6/2020']
dt_list = [datetime.strptime(x, '%m/%d/%Y') for x in date_str]
print(dt_list)

# 为了不用每次编写格式定义，使用第三方包dateutil中的parser.parse方法（pandas中已经自动安装好了）
from dateutil.parser import parse

parse('2016-08-29')
parse('Jan 31, 1997 10:45 PM')
parse('6/5/2020', dayfirst=True)  # 日出现在月的前面，传入dayfirst=True

# pandas采用to_datetime方法处理成组日期的
import pandas as pd

date_str = ['2020-05-09 12:00:00', '2011-08-06 00:00:00']
pd.to_datetime(
    date_str)  # DatetimeIndex(['2020-05-09 12:00:00', '2011-08-06 00:00:00'], dtype='datetime64[ns]', freq=None)

# 处理缺失值（None、空字符串等）

idx = pd.to_datetime(date_str + [None])

print(idx)
print(idx[2])  # NaT（Not a Time）是pandas中时间戳数据的null值
pd.isnull(idx)  # 返回数组array([False, False,  True], dtype=bool)
