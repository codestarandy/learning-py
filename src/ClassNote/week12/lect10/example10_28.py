"""
DateFrame.groupby()数据分组
"""

# example10_28.py

import pandas as pd

df = pd.DataFrame({'Animal': ['Falcon', 'Falcon',
                              'Parrot', 'Parrot'],
                   'Max Speed': [380., 370., 24., 24.]})
print(df)

print(df.groupby(['Animal']).mean())
print(df.groupby(['Max Speed']).size())
