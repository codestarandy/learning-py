"""
duplicated()判断是否为重复项
"""
# example10_25.py

import pandas as pd

s = pd.Series([1, 1, 1, 1, 2, 2, 2, 3, 4, 5, 5, 5, 5])
print(s.duplicated())  # 重复项判断
# 得到重复值判断的布尔值，观察布尔值为False的既为非重复值
print(s[s.duplicated() == False])

# 采用drop_duplicates()去除重复值，返回唯一值
print(s.drop_duplicates())
f = pd.DataFrame({'Q1': [1, 3, 4, 4, 4], 'Q2': [2, 3, 2, 2, 3], 'Q3': [1, 5, 2, 4, 4]})
print(f)
print(f.duplicated())
print(f.Q1.duplicated())
