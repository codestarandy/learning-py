# example10_18.py
# coding=utf-8
"""
读取stock.xlsx文件中的股票数据，分别实现按最高价升序排列、按最高价升序的基础上实现
开盘价升序排列、按最高价降序排列。
"""
import pandas as pd

df = pd.read_excel('stock.xlsx', sheet_name='stock',
                   index_col='Date', usecols='A:E', nrows=10)
print('排序前的DataFrame对象：\n', df)

# 以最高价来排序，默认为升序
df2 = df.sort_values(by=['High'])
print('以最高价升序排列后的DataFrame：\n', df2)
# 原来的df保持不变
print('原DataFrame保持不变：\n', df)

# 以最高价排序，如果最高价相同则再按开盘价排序
df3 = df.sort_values(by=['High', 'Open'])
print('以最高价和开盘价升序排列后的DataFrame：\n', df3)

# 按最高价排序; ascending=False降序
df4 = df2.sort_values(by=['High'], ascending=False)
print('以最高价降序排列后的DataFrame：\n', df4)
