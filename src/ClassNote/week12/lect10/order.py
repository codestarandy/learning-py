"""
    案例：年度销售数据分析V1.0
    功能：1.提取2019年度的商品销售数据；
         2.处理与业务流程不符合数据，具体包括：
         (1)支付时间早于下单时间，
         (2)支付时间间隔过长（大于30分钟），
         (3)订单金额与支付金额为负数的数据。

"""

# # 一.加载数据

# 1.加载处理数据所需要的库
import pandas as pd

# 2.读取数据文件
df = pd.read_excel('order2019.xlsx', index_col='id')
print(df)  # 104557行

# # 二.提取数据

# # 2.1根据业务需要提取数据,提取2019年数据 
# 1.引入时间模块, 确定周期时间
import datetime

startTime = datetime.datetime(2019, 1, 1)
endTime = datetime.datetime(2019, 12, 31, 23, 59, 59)
# 2.将数据源中的时间数据转换成datetime形式
df.orderTime = pd.to_datetime(df.orderTime)
df.payTime = pd.to_datetime(df.payTime)
# 3.将2019年1月1日前数据删除
df[df.orderTime < startTime]
df.drop(index=df[df.orderTime < startTime].index, inplace=True)
# 4.将2019年12月31日后数据删除
df[df.orderTime > endTime]
df.drop(index=df[df.orderTime > endTime].index, inplace=True)
df  # 104296行

# 2.2提取数据时,处理与业务流程不符合数据,支付时间间隔过长
# 1.下单时间与支付时间间隔
df['payinterval'] = (df.payTime - df.orderTime).dt.total_seconds()
# 2.支付时间间隔大于30分钟与支付时间早于下单时间
df[df.payinterval > 1800]
df.drop(index=df[df.payinterval > 1800].index, inplace=True)
df.drop(index=df[df.payinterval < 0].index, inplace=True)
df  # 103354行

# 2.3提取数据时,处理与业务流程不符合数据,订单金额与支付金额为负

df[df.orderAmount < 0]
df[df.payment < 0]
df.drop(index=df[df.payment < 0].index, inplace=True)
df  # 103348

# 三.清洗数据

# 3.1 查看数据

# In[46]:


df.info()
df.describe()

# 3.2 清洗orderID

# In[47]:


df.orderID.unique().size
# duplicated() duplicated()得到重复值判断的布尔值，再选择布尔值为False的既为非重复值
df.drop(index=df[df.orderID.duplicated()].index, inplace=True)
df.orderID.unique().size
df.info()  # 103321

# 3.3 清洗userID

# In[7]:


df.userID.unique().size  # 78525
# df.info() #103321


# # 3.4 清洗goodsID 

# In[48]:


df.goodsID[df.goodsID == 'PR000000'].size
df.drop(index=df[df.goodsID == 'PR000000'].index, inplace=True)
df.info()  # 103146

# # 3.5 清洗chanelID

# In[49]:


# 1.查看chanelID空值
df[df.chanelID.isnull()]
# 2.对空值进行修补
df['chanelID'].fillna(value=df.chanelID.mode()[0], inplace=True)
df.info()  # 103146

# # 3.6 清洗platformtype

# In[50]:


df.platfromType.unique().size  # 11
df.platfromType.unique()
df['platfromType'] = df['platfromType'].map(str.strip)
df['platfromType'] = df['platfromType'].str.replace(" ", "")
df.platfromType.unique()

# # 3.7 清洗payment

# In[51]:


# 1.创建折扣字段
df['discount'] = (df.payment / df.orderAmount)
df.describe()
# 2.平均折扣
meanDiscount = df[df['discount'] <= 1].discount.sum() / df[df['discount'] <= 1].discount.size
meanDiscount
# 3.找到折扣大于1的数据
df[df['discount'] > 1]
df['payment'] = df['payment'].mask(df['discount'] > 1, None)
# 4.对折扣大于1的数据进行填补
df.info()
df['payment'].fillna(value=df.orderAmount * meanDiscount, inplace=True)
# 5.处理折扣
df['discount'] = round((df.payment / df.orderAmount), 2)
df
# df.info() #103146


# # 3.8清洗结束 查看数据

# In[52]:


df.describe()

# # 至此以后,开始分析

# # 四.分析数据

# # 4.1.分析数据,整体销售情况

# In[53]:


# 总体概览
# 双十一 销售额  1 ,
# 瑞幸 
# 1.销售GMV
df.orderAmount.sum() / 10000  # 成交金额10835.088383万
# # # 2.成交总和
df.payment.sum() / 10000  # 销售额10246.203461009496万
# # # 3.实际成交额 
df[df.chargeback == "否"].payment.sum() / 10000  # 实际销售额8879.671990372466万
# # # 4.订单数量
df.orderID.unique().size  # 成单数 103146数
# # # 5.退货订单数
df[df.chargeback == "是"].orderID.size  # 实际成单89554
# # # 6.退货率 
df[df.chargeback == "是"].orderID.size / df.orderID.unique().size
# # 7.用户数
df.userID.unique().size  # 78525

# # 4.2销售情况,各月份GMV\成交额趋势

# In[54]:


# 翻转维度，以月份为坐标轴
# 1.月份
# df.字段名  获取字段数据   df ['字段名'] 写入数据
df['month'] = df['orderTime'].dt.month
df
# 2.绘制图
# 可视化 必备功课, 分析
import matplotlib.pyplot as plt
from matplotlib import font_manager

# get_ipython().run_line_magic('matplotlib', 'inline')
# mac fc-list : lang =zh
my_font = font_manager.FontProperties(fname='C:\Windows\Fonts\msyh.ttc', size=12)
# 图像大小
plt.figure(figsize=(15, 9))

plt.grid(alpha=0.4)

# GMV
x = df.groupby('month')['orderAmount'].sum().index
x
y1 = df.groupby('month')['orderAmount'].sum().values / 10000
# #销售实际付款 
y2 = df.groupby('month')['payment'].sum().values / 10000
# #不含退单销售额 
y3 = df[df.chargeback == "否"].groupby('month')['payment'].sum().values / 10000
x_ticks_label = ["{}月份".format(i) for i in x]
plt.xticks(x, x_ticks_label, rotation=45, fontproperties=my_font)

# plot 
plt.plot(x, y1, label='GMV', color="red", marker='o')
plt.plot(x, y2, label='销售额', color="orange", marker='*')
plt.plot(x, y3, label='不含退单', color="blue", marker='.')

plt.xlabel('月份', fontproperties=my_font)
plt.ylabel("销售额万元", fontproperties=my_font)
plt.title('销售额走势', fontproperties=my_font, color='red', size=20)
# 折点 添加坐标
for a, b in zip(x, y1):
    plt.annotate('(%.2f)' % (b), xy=(a, b), xytext=(-10, 10), textcoords='offset points')
# 图例
plt.legend(prop=my_font, loc='upper left')
plt.show()

# # 4.3 流量渠道来源分析

# In[55]:


# color red #223344 rgb(0,0,0)
custom = df.groupby('chanelID')['userID'].count()
plt.rcParams['font.sans-serif'] = ['SimHei']
custom.plot.pie(figsize=(20, 9), labels=custom.index, autopct="%1.1f%%")
plt.title('各渠道来源用户占比')

# # 4.4 用户行为, 研究周一到周日哪天订单量最高

# In[56]:


# 1.处理周几字段
# 0对应周一
df['dayofweek'] = df['orderTime'].dt.dayofweek
df['dayofweek'].unique()
# df
# 2.引入numpy
import numpy as np

# 3.按周几做聚合
week = df.groupby('dayofweek')['orderID'].count()
week
# #0是周一 
weekX = ['周一', '周二', '周三', '周四', '周五', '周六', '周日']

weekY = week.values
plt.xticks(range(len(weekX)), weekX, fontproperties=my_font)
# 柱状图\条形图
rects = plt.bar(range(len(weekX)), weekY, width=0.3, color=['r', 'g', 'b', 'r', 'g', 'b'])

for rect in rects:
    height = rect.get_height()
    plt.text(rect.get_x() + rect.get_width() / 2, height + 0.5, str(height), ha="center")
plt.show()

# # 4.5 用户行为, 哪个时间段下单量最高

# In[58]:


df1 = df.copy()
df1['orderTime'] = df1['orderTime'].dt.time
df1
tiemdf = df1.groupby('orderTime')['orderID'].count()

plt.figure(figsize=(20, 8), dpi=80)
s = df1['orderTime'].dt.floor('30T')
df1['orderTime'] = s.dt.strftime('%H:%M') + '-' + (s + pd.Timedelta(29 * 60, unit='s')).dt.strftime('%H:%M')
tiemdf = df1.groupby('orderTime')['orderID'].count()
tiemdf
tiemdfX = tiemdf.index
tiemdfY = tiemdf.values
tiemdfY
plt.style.use('ggplot')
plt.xticks(range(len(tiemdfX)), tiemdfX, rotation=90)
rect = plt.bar(tiemdfX, tiemdfY, width=0.3, color=['orange'])

# # 4.6用户行为,客户情况 

# In[59]:


# 1.客单价
df.orderAmount.sum() / df.userID.unique().size

# In[60]:


# 2.处理一下userID字段,后期要作为索引
df['userid'] = df["userID"].str[0:4]
df['userid'].unique()
# 3.userID只保留数字
df['userID'] = df["userID"].str[5:]
df

# # 4.7用户行为,客户复购率

# In[20]:


# 分析复购率和回购率
# 首先将用户消费数据进行数据透视。
# 希望统计每个用户在每月的订单量，所以userID是index，month是column。
pivoted_counts = df.pivot_table(index='userID', columns='month',
                                values='orderTime', aggfunc='count').fillna(0)
pivoted_counts.head()

# In[103]:


# 复购率

# 复购率的定义是在某时间窗口内消费两次及以上的用户在总消费用户中占比。这里的时间窗口是月，如果一个用户在同一天下了两笔订单，这里也将他算作复购用户。

# 将数据转换一下，消费两次及以上记为1，消费一次记为0，没有消费记为NaN。
# if x>1 :
#     x = 1 
# else :
#    if x==0:
#     np.NaN 
#    else :
#      0
import numpy as np

pcRepeatBuy = pivoted_counts.applymap(lambda x: 1 if x > 1 else np.NaN if x == 0 else 0)
pcRepeatBuy.head()
(pcRepeatBuy.sum() / pcRepeatBuy.count()).plot(figsize=(20, 9))
# 用sum和count相除即可计算出复购率。count是总的消费用户数，sum是两次以上的消费用户数。
# 3%  100  3


# # 4.8用户行为,客户RFM模型

# In[23]:


# 复制df数据
customdf = df.copy()
customdf.drop(index=df[df.chargeback == '是'].index, inplace=True)
customdf
customdf['orderTime'] = pd.to_datetime(customdf['orderTime'], format='%Y-%m-%d')  # 将字符串转换为日期格式
customdf
# customdf.set_index('userID',drop=False,inplace=True) 
# customdf
customdf.set_index('userID', drop=True, inplace=True)
customdf['orders'] = 1  # 计算原始订单频率
customdf
rfmdf = customdf.pivot_table(index=['userID'],
                             values=['orderAmount', 'orderTime', 'orders'],
                             aggfunc={'orderTime': 'max',
                                      'orderAmount': 'sum',
                                      'orders': 'sum'})
# rfmdf
rfmdf['R'] = (rfmdf.orderTime.max() - rfmdf.orderTime).dt.days

# #重命名列名
rfmdf.rename(columns={'orderAmount': 'M', 'orders': 'F'}, inplace=True)
rfmdf.head()

# In[24]:


rfmdf[['R', 'F', 'M']].apply(lambda x: x - x.mean())


# 对用户分类，设置标签
def rfm_func(x):
    level = x.apply(lambda x: "1" if x >= 1 else '0')
    label = level.R + level.F + level.M
    d = {
        '111': '重要价值客户',
        '011': '重要保持客户',
        '101': '重要发展客户',
        '001': '重要挽留客户',
        '110': '一般价值客户',
        '010': '一般保持客户',
        '100': '一般发展客户',
        '000': '一般挽留客户'
    }
    result = d[label]
    return result


rfmdf['label'] = rfmdf[['R', 'F', 'M']].apply(lambda x: x - x.mean()).apply(rfm_func, axis=1)
rfmdf.groupby('label').count()

# In[25]:


rfmdf.label.value_counts().plot.bar(figsize=(20, 9))
plt.xticks(rotation=0, fontproperties=my_font)

# In[ ]:
