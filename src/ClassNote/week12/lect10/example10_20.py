# example15_20.py
# coding=utf-8
import pandas as pd

# 打开文件
data = pd.read_excel('score.xlsx', index_col='姓名', usecols='B:I')
print('成绩DataFrame对象：\n', data)

# 对所有课程求基本统计量
df = data.describe()
print('所有课程成绩基本统计信息：\n', df)
print('《Java程序设计》成绩基本统计信息：\n', df['Java程序设计'])
print('各门课程的平均成绩：\n', df.loc['mean'])

# 对一门课程求基本统计量
print('《Java程序设计》成绩基本统计信息：\n', data.loc[:, 'Java程序设计'].describe())

# 可以单独求某一行（axis=1或columns）或一列（axis=0或index）的统计量
print('每个人的平均分：\n', data.mean(axis='columns'))
print('每个人的成绩标准差：\n', data.std(axis=1))
print('每门课程的平均分：\n', data.mean(axis='index'))
print('每门课程的成绩标准差：\n', data.std(axis=0))
# axis默认为0或index(按列统计)
print('每门课程的平均分：\n', data.mean())
print('每门课程的成绩标准差：\n', data.std())
