# example15_21.py
# coding=utf-8
import pandas as pd

# 打开文件
data = pd.read_excel('score.xlsx', index_col='姓名', usecols='B:I')
print('成绩DataFrame对象：\n', data)

# 所有课程之间的相关系数
print('所有课程之间的相关系数：\n', data.corr())

# 部分课程之间的相关系数
print('部分课程之间的相关系数：\n',
      data.loc[:, ['线性代数', '数据结构', 'Java程序设计']].corr())

# 两门课程之间的相关系数
print('两门课程之间的相关系数：\n',
      data.loc[:, ['线性代数', '数据结构']].corr())
print('两门课程之间的相关系数：',
      data['线性代数'].corr(data['数据结构']))
