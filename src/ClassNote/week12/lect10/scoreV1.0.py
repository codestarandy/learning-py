import pandas as pd

'''
案例：期末考试成绩数据分析V1.0

功能要求：
导入rz.xlsx文件为pandas数据类型
进行数据清洗（处理数据中的重复值、异常值、空值、多余的空格）
'''

# 数据导入
df = pd.read_excel(r'rz.xlsx')
print(df.shape)  # 查看数据“形状”

# 查找数据中的重复行，并将其删除
print(df.duplicated())  # 查找重复行

print(df[df.duplicated()])  # 查看重复行

df1 = df.drop_duplicates()  # 删除重复行
print(df1.shape)

# 查看空数据，并以0填充

# 查看空数据所在列代码
print(df1.isnull().any())  # 判断哪些列存在缺失值

# 显示存在缺失值行的代码：
print(df1[df1.isnull().values == True])

# 将空数据填充为0的代码
df2 = df1.fillna(0)
print(df2)  # 查看数据

# 处理数据中的空格
df0 = df2.copy()  # 为了数据安全，先copy一份
df0['解几'] = df2['解几'].astype(str).map(str.strip)

# 查看列数据类型
for i in list(df0.columns):
    if df0[i].dtype == 'O':  # 若某列全部是int，则显示该列为Int列型，否则为object
        print(i)

# 查看‘解几’列数据类型
print(df0['解几'].dtype)

# 将其转换为int 类型代码：
df0['解几'] = df2['解几'].astype(int)
print(df0['解几'].dtype)

# 以0填充体育列非int型数据
ty = list(df0.体育)
j = 0
for i in ty:
    if type(i) != int:
        # 判断“体育”列中的数据是否均为int类型                        
        print('第' + str(ty.index(i)) + '行有非int数据：', i)  # 若不是，则打印非int值及其所在的行号
        ty[j] = 0  # 用0替换该非int格式的值
    j = j + 1

print(ty)

df0['体育'] = ty  # 将替换过的List放回原df0中

print(df0['体育'])

# 以0填充军训列非int型数据
jx = list(df0.军训)
k = 0
for i in jx:
    if type(i) != int:
        print('第' + str(jx.index(i)) + '行有非 int 数据：', i)
        jx[k] = 0
    k = k + 1

df0['军训'] = jx
print(df0)
