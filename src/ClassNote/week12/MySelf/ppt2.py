# %%
import os

# 讲os.getcwd()返回的路径默认\改成/
cur = os.getcwd().replace("\\", '/')
# 更改文件路径
os.chdir(cur + "/src/ClassNote/week12/MySelf")
print(os.getcwd())

# %%
import pandas as pd
import numpy as np

df = pd.read_csv("files/stock.csv", sep=",",
                 index_col='Date', nrows=5,
                 usecols=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])

print(df)

# %%
df = pd.read_excel('files/stock.xlsx', sheet_name='stock',
                   index_col='Date', nrows=5, usecols='A:E,G')
# 输出来看看是什么
print(df)

# %%
df = pd.read_excel("files/stock.xlsx", sheet_name='stock', usecols='A:E,G')

print('开盘价大于13的数据行: \n', df[df['Open'] > 13])
print('开盘价最高的数据行:\n', df[df.Open == max(df['Open'])])
print('收盘价位于[12.5, 13.5]区间的数据行: \n', df[df.Close.between(12.5, 13.5)])
print('开盘价或收盘价为空值的数据航:\n', df[df.Open.isnull() | df.Close.isnull()])

# %%
"""
两种随机取样的方法
"""
i = 3
r = np.random.randint(0, len(df), i)
print('随机产生的行号为: ', r)
print('随机选取的{}行数据为:\n {}'.format(i, df.loc[r, :]))

# %%
# False指不可放回
print(df.sample(3, replace=False))

# %%
import pandas as pd

"""
读取stock.xlsx中股票相关数据，分别实现按列名，索引，最高价列数据进行排序
"""
df = pd.read_excel("src/ClassNote/week12/MySelf/files/stock.xlsx",
                   sheet_name='stock', index_col="Date",
                   usecols='A:E,G', nrows=10)

print('排序前的DataFrame对象:\n', df)

# 以最高价来排,默认为升序
df2 = df.sort_values(by=['High'])

print('以最高价升序排序: \n', df2)

print('原DataFrame对象保持不变:\n', df)

# 按列名标签排序
df3 = df.sort_index(axis=1)
print('按列名升序排列后的DateFrame:\n', df3)

# 默认axis=0, 按索引排序；ascending=False降序
df4 = df2.sort_index(ascending=False)
print('按索引降序排序后的DataFrame: \n ', df4)

# %%
# 最高价排序，如果最高价相同再按开盘价排序
df5 = df.sort_values(by=['High', 'Open'])
print('以最高价和开盘价升序排序后的DataFrame: \n', df5)

df5 = df.sort_values(by=['High', 'Open'], ascending=[False, False])
print('以最高价和开盘价降序排序后的DataFrame: \n', df5)

# %%
import pandas as pd

df = pd.read_excel('files/stock.xlsx',
                   sheet_name='stock', index_col='Date',
                   usecols='A:E', nrows=5)
print('原始的DataFrame对象:\n', df)
# 计算每天最高价与最低价的算术平均值
result = (df.High + df.Low) / 2
df['mean'] = result
print('添加平均价格后的DataFrame:\n ', df)
