# %%
import pandas as pd

data = {'province': ['广东', '山东', '河南', '四川', '江苏', '河北', '湖南', '安徽', '湖北', '浙江'],
        'population': [10999, 9946, 9532, 8262, 7998, 7470, 6822, 6195, 5885, 5590],
        'city': ['广州', '济南', '郑州', '成都', '南京', '石家庄', '长沙', '合肥', '武汉', '杭州']}
cc = pd.DataFrame(data)
print(cc)

# %%
# 加了area
cc = pd.DataFrame(data, columns=['province', 'population', 'city', 'area'])
print(cc)

# %%
print(cc.index)
print('分割线'.center(50, '*'))
print(cc.columns)

# %%
print(cc.dtypes)
print('分割线'.center(50, '*'))
print(cc.info())

# %%
print(cc.values)

# %%
print(cc['city'])

# %%
print(cc['city'][6])

# %%
province = cc.set_index('province')
print(province)

# %%
print(province['population'])

# %%
print(cc[cc['population'] > 8000])

# %%
print(province[['population', 'city']])

# %%
print(province.loc["山东":"安徽"])

# %%
print(province.loc[:, :'population'])

# %%
print(province.iloc[2:6])

# %%
print(province.iloc[2:8:2, 1:2])

