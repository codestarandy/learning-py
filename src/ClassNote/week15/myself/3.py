import requests
from bs4 import BeautifulSoup

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    html.encoding = html.apparent_encoding
    soup = BeautifulSoup(html.text, 'lxml')
    universities = soup.select('table tbody tr')
    title = '  排名  ' + '大学'.center(11, chr(12288)) + '省市'.center(6, chr(12288)) + '  总分  '
    print(title)
    result = [title + '\n']
    for university in universities:
        items = university.select('td')[:5]
        item_str = items[0].text.center(6, ' ')
        item_str += items[1].text.center(12, chr(12288))
        item_str += items[2].text.center(6, chr(12288))
        item_str += items[4].text.center(6, ' ')
        print(item_str)
        result.append(item_str + '\n')
    return result


if __name__ == '__main__':
    url = 'http://www.zuihaodaxue.com/zuihaodaxuepaiming2020.html'
    school = get_data(url)
    with open('files/universities.txt', 'w') as f:
        f.writelines(school)
