import requests
from bs4 import BeautifulSoup

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Referer': 'https://music.163.com/'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    html.encoding = html.apparent_encoding
    soup = BeautifulSoup(html.text, 'lxml')
    songs = soup.select('table tbody tr td .txt b')
    for song in songs:
        print(song)


if __name__ == '__main__':
    get_data('https://music.163.com/#/discover/toplist?id=19723756')