import requests
from bs4 import BeautifulSoup
from datetime import datetime

headers_MEdge = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'cookie': 'HMACCOUNT=1FD155E87CF7BFCA; BAIDUID=BF0E0DA0756D6EF364FDCBD41CAB2E65:FG=1; jshunter-uuid=f798cbf9-5ea8-46fb-a508-b7d191b97780; BDUSS=xaNG9iY3hlUVR1M01PRTJ-bjdDb0VsalJHN2R0eWt2SXRlNGU4aGxmalBEOUZlSVFBQUFBJCQAAAAAAAAAAAEAAAC625zZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM-CqV7PgqleQ0; BIDUPSID=BF0E0DA0756D6EF364FDCBD41CAB2E65; PSTM=1589270983; H_PS_PSSID=31360_1467_31671_21117_31780_31673_30824_31848_26350'
}

headers_Chrome = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
}


def get_data_MEdge(url, now):
    now_str = str(now)
    title = '从Microsoft Edge浏览器访问百度\n时间: ' + now_str + '\n'
    result = [title]

    html = requests.get(url, headers=headers_MEdge)
    html.encoding = html.apparent_encoding
    soup = BeautifulSoup(html.text, 'lxml')
    news = soup.select('.title-content-title')
    i = 0
    for new in news:
        i += 1
        new_str = format(str(i), ' <3') + new.text
        result.append(new_str + '\n')

    path = '../files/baiduNews_MEdge' + \
           str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '.txt'
    with open(path, 'w') as f:
        f.writelines(result)


def get_data_Chrome(url, now):
    now_str = str(now)
    title = '从google浏览器访问百度\n时间: ' + now_str + '\n'
    result = [title]

    html = requests.get(url, headers=headers_Chrome)
    html.encoding = html.apparent_encoding
    soup = BeautifulSoup(html.text, 'lxml')
    news = soup.select('.title-content-title')

    temp = []
    i = 0
    length_news = len(news)

    for new in news:
        # print(new.text)
        i += 1
        if i % 2 == 1:
            new_str = format(str(int((i + 1) / 2)), ' <3') + new.text
            result.append(new_str + '\n')
        else:
            new_str = format(str(int(i / 2 + length_news / 2)), ' <3') + new.text
            temp.append(new_str + '\n')

    for item in temp:
        result.append(item)

    # 输出查看
    # for i in result:
    #     print(i, end='')

    path = '../files/baiduNews_Chrome' + \
           str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '.txt'
    with open(path, 'w') as f:
        f.writelines(result)


def read_files(path):
    with open(path, 'r') as f:
        data = f.readlines()
    for item in data:
        print(item.replace('\n', ''))


if __name__ == '__main__':
    now = datetime.now()
    url = 'https://www.baidu.com/'
    get_data_Chrome(url, now)
    get_data_MEdge(url, now)

    path_M = '../files/baiduNews_MEdge' + \
             str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '.txt'
    path_C = '../files/baiduNews_Chrome' + \
             str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '.txt'

    read_files(path_C)
    print('分割线'.center(60, '*'))
    read_files(path_M)
