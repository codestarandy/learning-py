import requests
from bs4 import BeautifulSoup

headers = {
    # 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36 Edg/83.0.478.45',
    # 'cookie': 'SRCHD=AF=NOFORM; SRCHUID=V=2&GUID=43F8A09F4E9E44F3BAB0F7A84A0F6C1D&dmnchg=1; ANON=A=4EC5924FBEAB65EEFCC5FC7EFFFFFFFF; _EDGE_V=1; MUID=2472BC5AE96961F9302CB2E5E847603E; MUIDB=2472BC5AE96961F9302CB2E5E847603E; ULC=P=C7B0|1:1&H=C7B0|1:1&T=C7B0|1:1; ENSEARCH=BENVER=0; _tarLang=default=en; _TTSS_OUT=hist=WyJlbiJd; _TTSS_IN=hist=WyJ6aC1IYW5zIiwiYXV0by1kZXRlY3QiXQ==; ABDEF=V=0&ABDV=0&MRNB=1591617607308&MRB=0; WLS=C=f8f66f0cfa86aeae&N=%e7%83%81%e6%98%8e; _SS=SID=2933D4DFC90F690E0419DA3CC82168D8; _U=16lUddSCtm2mmaoleZZ-WsAViHr2xVMmAqKcpEqtxxw8cS_z1KnMhcBpSU9Hv86STWTwQu0HmfJbEVIFUA0JeCsnKOnQDNDZ0hqCZ0srYM0jh2Fu4wkzAF-0eSYP9SBp1OszQSBJPVSAODH9PkrQRa6xJ14e8RM32e4kZsuLVMVAJygVczNBJKdSzPz9BTG4rko2yWvBvATlX9z36hwyY6Q; _EDGE_S=mkt=zh-cn&SID=2933D4DFC90F690E0419DA3CC82168D8; SNRHOP=I=&TS=; SRCHUSR=DOB=20200429&T=1591629966000; SRCHHPGUSR=CW=889&CH=810&DPR=1&UTC=480&WTS=63723980150&HV=1591629970'
}


def get_html(url):
    html = requests.get(url, headers=headers)
    if html.status_code == 200:
        print("获取页面成功")
        html.encoding = html.apparent_encoding
        return html.text
        pass
    else:
        print('error ', html.status_code)


if __name__ == '__main__':
    # url = 'https://cn.bing.com/search?q='
    # print(get_html(url + '百度'))
    url = 'https://www.baidu.com/'
    print(get_html(url))



# %%
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Cookie': 'BAIDUID=BF0E0DA0756D6EF364FDCBD41CAB2E65:FG=1; jshunter-uuid=f798cbf9-5ea8-46fb-a508-b7d191b97780; BDUSS=xaNG9iY3hlUVR1M01PRTJ-bjdDb0VsalJHN2R0eWt2SXRlNGU4aGxmalBEOUZlSVFBQUFBJCQAAAAAAAAAAAEAAAC625zZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM-CqV7PgqleQ0; BIDUPSID=BF0E0DA0756D6EF364FDCBD41CAB2E65; PSTM=1589270983; BDRCVFR[Fc9oatPmwxn]=aeXf-1x8UdYcs; H_PS_PSSID=31360_1467_31671_21117_31780_31673_30824_31848_26350'
}
kv = {'wd': 'python'}
url = 'https://www.baidu.com/s'
r = requests.get(url, params=kv, headers=headers)
print(r.url)
# print(r.encoding)
