import requests

url = 'https://www.baidu.com/'
r = requests.get(url)
print(type(r))
print(r.text)

# %%


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
    'Cookie': 'BIDUPSID=3E808FE52F0F7F217630F3CF8F1A8C74; PSTM=1586081722; BAIDUID=3E808FE52F0F7F21648D6CB936AAC487:FG=1; BD_UPN=12314353; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; BD_HOME=1; H_PS_PSSID=1425_31124_21090_31423_31270_31463_31322_30824_31164; delPer=0; BD_CK_SAM=1; PSINO=1; H_PS_645EC=719bjHZyEUxm9Jy9liX6NBPSHdlUT1iE4etV1oufwjbGFGkcPJJZkOjdTIk; BDSVRTM=113; COOKIE_SESSION=80882_3_2_5_5_3_0_0_0_3_75_0_0_0_0_0_1588772033_1588132884_1588929039%7C8%230_1_1588132791%7C1'
}
kv = {'wd': 'python'}
url = 'https://www.baidu.com/s'
r = requests.get(url, params=kv, headers=headers)
print(r.url)

# %%
print(r.text)


# %%
def get_html(url):
    try:
        r = requests.get(url, headers=headers)
        r.raise_for_status()  # 如果状态不是200，引发异常
        # r.encoding = r.apparent_encoding  #改编码方式
        return r.text
    except:
        print('爬取失败')
        return ''


url = 'https://item.jd.com/2967929.html'
print(get_html(url))
