import requests
from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    html.encoding = html.apparent_encoding
    # print(html.text)
    soup = BeautifulSoup(html.text, 'lxml')
    universities = soup.select('table tbody tr')
    # print(universities)
    print(' 排名 ' + '大学'.center(12, chr(12288)) + '省市'.center(6, chr(12288)) + ' 总分 ')
    for u in universities:
        items = u.select('td')[:4]
        item_list = []
        # for item in items:
        #     item_list.append(item.text)
        # item_str = ','.join(item_list)
        # print(item_str)
        item_str = items[0].text.center(6, ' ')
        item_str += items[1].text.center(12, chr(12288))
        item_str += items[2].text.center(6, chr(12288))
        item_str += items[3].text.center(6, ' ')
        print(item_str)


if __name__ == '__main__':
    # url = 'http://www.zuihaodaxue.com/zuihaodaxuepaiming2019.html'
    url = 'http://www.zuihaodaxue.com/zuihaodaxuepaiming2020.html'
    get_data(url)
