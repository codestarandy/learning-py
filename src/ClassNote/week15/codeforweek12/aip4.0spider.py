import requests
import json

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
    'Referer': 'http://fundf10.eastmoney.com/jjjz_160706.html'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    html_text = html.text
    start = html_text.find('{"Data":{"LSJZList"')
    json_data = json.loads(html_text[start:-1])
    netvalues = json_data['Data']['LSJZList']
    for data in netvalues:
        fsrq = data['FSRQ']
        dwjz = data['DWJZ']
        print(fsrq, dwjz)


if __name__ == '__main__':
    for i in range(1, 11):
        url = 'http://api.fund.eastmoney.com/f10/lsjz?callback=jQuery183019155807252835944_1589080488743' \
              '&fundCode=160706&pageIndex={}&pageSize=20&startDate=&endDate=&_=1589080488770'.format(i)
        print('当前正在输出第{}页...'.format(i))
        get_data(url)
