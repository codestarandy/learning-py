import requests
import re
from bs4 import BeautifulSoup
import os

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
}
# "excerptArea":{"text":"男子强奸 10 岁「百香果女童」致死，二审死刑改判死缓"}
# "link":{"url":"https:\u002F\u002Fwww.zhihu.com\u002Fquestion\u002F393501523"}
content_re = re.compile('"excerptArea":{"text":"(.*?)"}')
url_re = re.compile('"link":{"url":"(.*?)"}')


def get_data(url):
    html = requests.get(url, headers=headers)
    soup = BeautifulSoup(html.text, 'lxml')
    titles = soup.select('.HotList-itemTitle')
    for title in titles:
        print('标题：', title.text)
    print('-' * 50)

    imgs = soup.select('.HotList-itemImgContainer img')
    for img in imgs:
        print('图片链接：', img['src'])
        downloadimg(img['src'])
    print('-' * 50)

    html_text = html.text
    contents = content_re.findall(html_text)
    for c in contents:
        print('内容：', c)

    print('-' * 50)
    urls = url_re.findall(html_text)
    for u in urls:
        print('链接：', u)


def downloadimg(img):
    filepath = 'zhihuimg'
    if not os.path.exists(filepath):
        os.mkdir(filepath)
    html = requests.get(img, headers=headers)
    with open(filepath + '/' + img.split('/')[-1], 'wb') as f:
        f.write(html.content)


if __name__ == '__main__':
    url = 'https://www.zhihu.com/billboard'
    get_data(url)
