# %%
from datetime import datetime, timedelta

now = datetime.now()
print(now)
print(now.year)
print(now.month)
print(now.day)

# %%
delta = datetime(2020, 5, 7) - datetime(2019, 2, 24, 8, 15)
print(delta)

# %%
start = datetime(2019, 2, 24)
print(start + timedelta(365))
print(start - 2 * timedelta(365))

# %%
stamp = datetime(2020, 5, 9)
print(str(stamp))
print(stamp.strftime('%Y-%m-%d'))

# %%
date = '2020-05-09'
dt = datetime.strptime(date, '%Y-%m-%d')
print(dt)

