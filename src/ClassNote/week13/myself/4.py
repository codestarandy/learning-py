# %%
from datetime import datetime
import pandas as pd
import numpy as np

dates = [datetime(2011, 1, 2), datetime(2011, 1, 5),
         datetime(2011, 1, 7), datetime(2011, 1, 8),
         datetime(2011, 1, 10), datetime(2011, 1, 12)]
ts = pd.Series(np.random.randn(6), index=dates)
print(ts)
print(ts.index)

# %%
ts = ts + ts[::2]
print(ts)

# %%
print(ts.index.dtype)
stamp = ts.index[0]
print(stamp)

# %%
print(ts['1/10/2011'])
print(ts['20110110'])

# %%
print(ts[datetime(2011, 1, 7):])

# %%
print(ts)
print('分割线'.center(50, '*'))
print(ts['1/6/2011':'1/11/2011'])

# %%
dates = pd.date_range('1/1/2000', periods=100, freq='W-WED')
long_df = pd.DataFrame(np.random.randn(100, 4), index=dates,
                       columns=['Colorado', 'Texas', 'New York', 'Ohio'])
print(long_df.loc['5-2001'])

# %%
dates = pd.DatetimeIndex(['1/1/2000', '1/2/2000', '1/2/2000',
                          '1/2/2000', '1/3/2000'])
dup_ts = pd.Series(np.arange(5), index=dates)
print(dup_ts)

# %%
print(dup_ts.index.is_unique)
print(dup_ts['1/3/2000'])
print('分割线'.center(50, '*'))
print(dup_ts['1/2/2000'])
