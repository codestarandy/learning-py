"""
    案例：年度销售数据分析V1.0
    功能：1.提取2019年度的商品销售数据；
         2.处理与业务流程不符合数据，具体包括：
         (1)支付时间早于下单时间，
         (2)支付时间间隔过长（大于30分钟），
         (3)订单金额与支付金额为负数的数据。

"""
# %%
import os

print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week13/MySelf")
print(os.getcwd())

# %%
import pandas as pd
import datetime

# 读取数据文件
df = pd.read_excel('files/order2019.xlsx', index_col='id')
print(df)  # 104557行

# %%
startTime = datetime.datetime(2019, 1, 1)
endTime = datetime.datetime(2019, 12, 31, 23, 59, 59)
# 将数据源中的时间数据转换成datetime形式
df.orderTime = pd.to_datetime(df.orderTime)
df.payTime = pd.to_datetime(df.payTime)

print(df[df.orderTime < startTime])

# %%
# 将2019年1月1日前数据删除
df.drop(index=df[df.orderTime < startTime].index, inplace=True)
print(df)
print(df[df.orderTime > endTime])

# %%
# 将2019年12月31日后数据删除
df.drop(index=df[df.orderTime > endTime].index, inplace=True)
print(df)  # 104296行

# %%
# 2.提取数据时,处理与业务流程不符合数据

# 计算下单时间与支付时间间隔
df['pay_interval'] = (df.payTime - df.orderTime).dt.total_seconds()
# (1) 支付时间间隔大于30分钟
print(df[df.pay_interval > 1800])
# 把支付时间间隔大于30分钟的给删掉
df.drop(index=df[df.pay_interval > 1800].index, inplace=True)
print(df)

# %%
# (2) 支付时间早于下单时间
df.drop(index=df[df.pay_interval < 0].index, inplace=True)
print(df)  # 103354行

# %%
# (3) 订单金额与支付金额为负
print(df[df.orderAmount < 0])
print(df[df.payment < 0])

# %%
df.drop(index=df[df.payment < 0].index, inplace=True)
print(df)  # 103348

# %%
# 查看数据
print(df.info())
print('分割线'.center(50, '*'))
print(df.describe())
