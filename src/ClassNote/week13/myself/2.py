# %%
import pandas as pd

date_str = ['2011-01-06 12:00:00', '2011-08-06 00:00:00']
idx = pd.to_datetime(date_str, format='%Y-%m-%d')
print(idx)

# %%
import pandas as pd

date_str = ['2011-01-06 12:00:00', '2011-08-06 00:00:00']
idx = pd.to_datetime(date_str + [None], format='%Y-%m-%d')
print(idx)

# %%
print(idx[2])
print(pd.isnull(idx))
