"""
    案例：年度销售数据分析V2.0
    功能：1.提取2019年度的商品销售数据；
         2.处理与业务流程不符合数据，具体包括：
         (1)支付时间早于下单时间，
         (2)支付时间间隔过长（大于30分钟），
         (3)订单金额与支付金额为负数的数据。
    新增功能：
         3.进一步清洗数据，逐列检查，修补空值，填补缺失值。

"""
# %%
import os

print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week13/MySelf")
print(os.getcwd())

# %%
import pandas as pd
import datetime
import time

# 读取数据文件
start_time = time.time()
df = pd.read_excel('files/order2019.xlsx', index_col='id')
end_time = time.time()
print(df)  # 104557行
print('读取 {} 行数据花了 {:.2f} 秒'.format(len(df), end_time - start_time))

# %%
startTime = datetime.datetime(2019, 1, 1)
endTime = datetime.datetime(2019, 12, 31, 23, 59, 59)
# 将数据源中的时间数据转换成datetime形式
df.orderTime = pd.to_datetime(df.orderTime)
df.payTime = pd.to_datetime(df.payTime)

print(df[df.orderTime < startTime])

# %%
# 将2019年1月1日前数据删除
df.drop(index=df[df.orderTime < startTime].index, inplace=True)
print(df)  # 104552行
print(' df '.center(50, '*'))
# 显示晚于2019年12月31日23时59分59秒的订单
print(df[df.orderTime > endTime])

# %%
# 将2019年12月31日后数据删除
df.drop(index=df[df.orderTime > endTime].index, inplace=True)
print(df)  # 104296行

# %%
# 2.提取数据时,处理与业务流程不符合数据

# 计算下单时间与支付时间间隔
df['pay_interval'] = (df.payTime - df.orderTime).dt.total_seconds()
# (1) 支付时间间隔大于30分钟
print(df[df.pay_interval > 1800])
df.drop(index=df[df.pay_interval > 1800].index, inplace=True)
print('删除后'.center(50, '*'))
print(df)

# %%
# (2) 支付时间早于下单时间
df.drop(index=df[df.pay_interval < 0].index, inplace=True)
print(df)  # 103354行

# %%
# (3) 订单金额与支付金额为负
print(df[df.orderAmount < 0])
print(df[df.payment < 0])

# %%
df.drop(index=df[df.payment < 0].index, inplace=True)
print(df)  # 103348

# %%
# 查看数据
print(df.info())
print('分割线'.center(50, '*'))
print(df.describe())

# %%
# 3.清洗数据

# (1)清洗orderID
print(df.orderID.unique().size)  # 计算非重复项的长度
print(' 分割线 1 '.center(50, '*'))
df.drop(index=df[df.orderID.duplicated()].index, inplace=True)  # 删除重复值的行
print(df.orderID.unique().size)  # 重新计算非重复项的长度
print(' 分割线 2 '.center(50, '*'))
print(df.info())  # 103321

# %%
# (2)清洗userID
print(df.userID.unique().size)  # 计算非重复项的长度：78525
# df.info() #103321
# print('分割线'.center(50, '*'))

# %%
# (3)清洗goodsID
print(df.goodsID[df.goodsID == 'PR000000'].size)
print('分割线'.center(50, '*'))
df.drop(index=df[df.goodsID == 'PR000000'].index, inplace=True)  # 删除空缺行
print(df.info())  # 103146

# %%
# (4)清洗chanelID
# 查看chanelID空值
print(df[df.chanelID.isnull()])
print('分割线'.center(50, '*'))
# 对空值进行修补
df['chanelID'].fillna(value=df.chanelID.mode()[0], inplace=True)
print(df.info())  # 103146

# %%
# (5) 清洗platformtype
print(df.platfromType.unique().size)  # 11
print(' 分割线 1 '.center(50, '*'))
print(df.platfromType.unique())
print(' 分割线 2 '.center(50, '*'))
df['platfromType'] = df['platfromType'].map(str.strip)  # 删除字符串的头和尾的空格,以及位于头尾的\n\t符号
df['platfromType'] = df['platfromType'].str.replace(" ", "")  # 删除字符串中出现的空格
print(df.platfromType.unique())

# %%
# (6) 清洗payment
# a.创建折扣字段
df['discount'] = (df.payment / df.orderAmount)
print(df.describe())

# %%
# b.平均折扣
meanDiscount = df[df['discount'] <= 1].discount.sum() / df[df['discount'] <= 1].discount.size
print(meanDiscount)

# %%
# c.找到折扣大于1的数据
print(df[df['discount'] > 1])

# %%
# d.对折扣大于1的数据进行填补
df['payment'] = df['payment'].mask(df['discount'] > 1, None)
print(df.info())

# %%
df['payment'].fillna(value=df.orderAmount * meanDiscount, inplace=True)
# e.处理折扣
df['discount'] = round((df.payment / df.orderAmount), 2)
print(df)

# %%
print(df.info())  # 103146
print('分割线'.center(50, '*'))
# 查看数据
print(df.describe())
