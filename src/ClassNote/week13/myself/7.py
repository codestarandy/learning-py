# %%
import pandas as pd

obj = pd.Series(['c', 'a', 'd', 'a', 'a',
                 'b', 'b', 'c', 'c', 'c'])
print(obj.unique())

# %%
data = {'Q1': [1, 3, 4, 4, 4],
        'Q2': [2, 3, 2, 2, 3],
        'Q3': [1, 5, 2, 4, 4]}
frame = pd.DataFrame(data)
print(frame.Q1.unique())
print('分割线'.center(50, '*'))
print(frame.Q2.unique())
print('分割线'.center(50, '*'))
print(frame.Q3.unique())

# %%
import pandas as pd

s = pd.Series([1, 1, 1, 1, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5])
print(s.duplicated())
print(s[s.duplicated() == False])

# %%
# 返回不重复的值
print(s.drop_duplicates())

# %%
f = pd.DataFrame({'Q1': [1, 3, 4, 4, 4],
                  'Q2': [2, 3, 2, 2, 3],
                  'Q3': [1, 5, 2, 4, 4]})
print(f)
print(f.duplicated())
print(f.Q1.duplicated())

# %%
import pandas as pd

ser = pd.Series([4.5, 7.2, -5.3, 3.6],
                index=['d', 'b', 'a', 'c'])
print(ser.drop('c'))
print('注意看c那一行'.center(50, '*'))
print(ser)

# %%
import numpy as np

df = pd.DataFrame(np.arange(9).reshape(3, 3),
                  index=['a', 'c', 'd'], columns=['oh', 'te', 'ca'])
print(df)
print('分割线'.center(50, '*'))
print(df.drop('a'))
print('分割线'.center(50, '*'))
print(df.drop(['oh', 'te'], axis=1))
print(' df其实并没有变化 '.center(50, '*'))
print(df)

# %%
df = pd.DataFrame([[np.nan, 2, np.nan, 0],
                   [3, 4, np.nan, 1],
                   [np.nan, np.nan, np.nan, 5],
                   [np.nan, 3, np.nan, 4]],
                  columns=list('ABCD'))

print(df)

# %%
print('将NAN值转换为0')
print(df.fillna(0))

# %%
print('向前或向后传播')
print(df.fillna(method='ffill'))

# %%
print('用字典转换')
values = {'A': 0, 'B': 1, 'C': 2, 'D': 3}
print(df.fillna(value=values))

# %%
print('只替换第一个值')
print(df.fillna(value=values, limit=1))

# %%
frame = pd.DataFrame({
    'key1': ['a', 'b', 'c', 'd'],
    'key2': ['one', 'two', 'three', 'four'],
    'data1': np.arange(4),
    'data2': np.arange(5, 9)
})
print(frame)

# %%
# frame['data1'] = frame['data1'].map(lambda x: '%.3f' % x)
# 如果data1那一列已经是浮点数，则会报错,所以先判断一下
if type(frame['data1'][0]) != str:
    frame['data1'] = frame['data1'].map(lambda x: '{:.3f}'.format(x))
print(frame)

# %%
df = pd.DataFrame(np.arange(9).reshape(3, 3),
                  index=['a', 'c', 'd'], columns=['oh', 'te', 'ca'])
print(df)
print('分割线'.center(50, '*'))
print(df.mask(df > 3, None))
print('分割线'.center(50, '*'))
print(df)

# %%
df = pd.DataFrame(np.arange(9).reshape(3, 3),
                  index=['a', 'c', 'd'], columns=['oh', 'te', 'ca'])
print(df)
print('df某些元素被替代后'.center(50, '*'))
# df中 <= 3的元素将被替换成None
df.where(df > 3, None, inplace=True)
print(df)
