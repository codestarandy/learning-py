# %%
# DateFrame.merge()数据连接
import pandas as pd

df1 = pd.DataFrame({
    'l_key': ['foo', 'bar', 'baz', 'foo'],
    'value': [1, 2, 3, 4]
})

df2 = pd.DataFrame({
    'r_key': ['foo', 'bar', 'qux', 'bar'],
    'value': [5, 6, 7, 8]
})

print(df1)
print(df2)

# %%
df_1_2 = df1.merge(df2, left_on='l_key', right_on='r_key')
print(df_1_2)

# %%
# DateFrame.groupby()数据分组

df = pd.DataFrame({'Animal': ['Falcon', 'Falcon', 'Parrot', 'Parrot'],
                   'Max Speed': [380., 370., 24., 24.]})
print(df)

# %%
print(' group by Animal '.center(40, '*'))
print(df.groupby(['Animal']).mean())
print(' group by Max Speed '.center(40, '*'))
print(df.groupby(['Max Speed']).size())


# %%
print(df.groupby(['Animal']).sum())
