# %%
import pandas as pd
import numpy as np

longer_ts = pd.Series(100 * np.random.randn(1000),
                      index=pd.date_range('1/1/2000', periods=1000))
print(longer_ts)

# %%
print(longer_ts['2001'])
print(longer_ts['2001-05'])

# %%
