import requests
import json

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
    'Referer': 'http://fundf10.eastmoney.com/jjjz_160706.html'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    # print(html.text)
    html_text = html.text
    start = html_text.find('{"Data":{"LSJZList":')
    json_data = json.loads(html_text[start:-1])
    net_values = json_data['Data']['LSJZList']
    # print(json_data)
    # print(net_values)
    num = 1
    for data in net_values:
        # 日期
        date = data['FSRQ']
        # 单位性质
        properties = data['DWJZ']
        print(num, date, properties)
        num += 1


if __name__ == '__main__':
    for i in range(1, 11):
        url = 'https://api.fund.eastmoney.com/f10/lsjz?callback=jQuery18305677916123030269_1592322087404&fundCode=160706' \
              '&pageIndex={}&pageSize={}&startDate=&endDate=&_=1592322087416'.format(i, 20)
        print(' 当前输出第{}页 '.format(i).center(50, '*'))
        get_data(url)
