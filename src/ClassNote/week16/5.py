"""
需要注意的是，有些网速过慢的情况可能下载会失败。
"""

import requests
from contextlib import closing

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
}


def get_small_file(url, save_name):
    r = requests.get(url, headers=headers)
    with open('someFiles/' + save_name, 'wb') as f:
        f.write(r.content)


# 文件下载器
def get_big_file(url, save_path):
    with closing(requests.get(url, headers=headers, stream=True)) as response:
        chunk_size = 10240  # 单次请求最大值
        content_size = int(response.headers['content-length'])  # 内容体总大小
        data_count = 0
        with open('someFiles/' + save_path, "wb") as f:
            for data in response.iter_content(chunk_size=chunk_size):
                f.write(data)
                data_count = data_count + len(data)
                now_jd = (data_count / content_size) * 100
                print("\r 文件下载进度：%d%%(%d/%d) - %s" % (now_jd, data_count, content_size, save_path), end=" ")


def get_big_without_progress_tip(url, save_path):
    """
    为了应对返回的报文没有的情况
    """
    with closing(requests.get(url, headers=headers, stream=True)) as response:
        chunk_size = 10240  # 单次请求最大值
        with open('someFiles/' + save_path, "wb") as f:
            for data in response.iter_content(chunk_size=chunk_size):
                f.write(data)


def get_file_list(lt):
    """
    lt格式 [下载的网络链接, 下载保存的本地地址]
    :param lt: 文件下载列表
    :return: None
    """
    for item in lt:
        print(' 下载 {} 中'.format(item[1]).center(50, '*'))
        try:
            get_big_file(item[0], item[1])
        except:
            print('正在下载，因为文件没有返回长度信息，所以无法告知您进度...')
            get_small_file(item[0], item[1])
        print()
    print('-----> 所有文件都已经下载完毕  <-----')


if __name__ == '__main__':
    file_lt = [
        # ['https://download.jetbrains.8686c.com/cpp/CLion-2020.2.exe',
        #  'CLion-v2020.2.exe'],
        ['https://download.jetbrains.8686c.com/python/pycharm-professional-2020.2.exe',
         'Pycharm-v2020.2.exe'],
        ['https://download.jetbrains.8686c.com/idea/ideaIU-2020.2.exe',
         'IDEA-v2020.2.exe'],
        ['https://nodejs.org/dist/v12.18.3/node-v12.18.3-x64.msi',
         'nodejs-v12.18.3.msi'],
        # ['https://npm.taobao.org/mirrors/python/3.7.6/python-3.7.6.exe',
        #  'python-v3.7.6.exe']
    ]
    get_file_list(file_lt)
