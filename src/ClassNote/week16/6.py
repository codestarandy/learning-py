"""
配合flask使用, flask部署在阿里云上
"""
import requests


def draw_grade(grade, save_path):
    x = list(range(1, len(grade) + 1))
    x = [str(i) for i in x]
    data = dict(zip(x, grade))
    data['num'] = len(grade)
    print(data)
    url = 'http://47.115.162.94:8000/grade'
    res = requests.post(url=url, data=data)
    with open(save_path, 'wb') as f:
        f.write(res.content)


if __name__ == '__main__':
    test = (4.38, 4.08, 4.11, 4.08)
    draw_grade(test, 'someFiles/result.png')