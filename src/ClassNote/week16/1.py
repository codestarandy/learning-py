import requests
import re
from bs4 import BeautifulSoup
import os

# "excerptArea":{"text":"晒出你观察到的节目细节，安利你 pick 的姐姐，还有机会拿到姐姐签名照哦！戳这里，即刻参与：有奖视频活动：出道吧！姐姐 列文虎克们，来带着放大镜看吧！"}
# "link":{"url":"https:\u002F\u002Fwww.zhihu.com\u002Fquestion\u002F401056846"}
content_re = re.compile('"excerptArea":{"text":"(.*?)"}')
url_re = re.compile('"link":{"url":"(.*?)"}')

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
}


def get_data(url):
    html = requests.get(url, headers=headers)
    soup = BeautifulSoup(html.text, 'lxml')
    titles = soup.select('.HotList-itemTitle')
    for title in titles:
        print("标题: ", title.text)
    print('分割线'.center(50, '*'))
    imgs = soup.select('.HotList-itemImgContainer img')
    for img in imgs:
        print("图片链接: ", img['src'])
        # download_img(img['src'])
    print('分割线'.center(50, '*'))

    html_text = html.text
    contents = content_re.findall(html_text)
    print(len(contents))
    for c in contents:
        print("内容: ", c)
    print('分割线'.center(50, '*'))

    urls = url_re.findall(html_text)
    for u in urls:
        u = u.replace("u002F", '')
        print('链接: ', u)


def download_img(img):
    file_path = 'zhihu_img'
    if not os.path.exists(file_path):
        os.mkdir(file_path)
    pic = requests.get(img, headers=headers)
    with open(file_path + '/' + img.split('/')[-1], 'wb') as f:
        f.write(pic.content)


if __name__ == '__main__':
    url = 'https://www.zhihu.com/billboard'
    get_data(url)
