import requests
import re
from bs4 import BeautifulSoup
import os

content_re = re.compile('"excerptArea":{"text":"(.*?)"}')
url_re = re.compile('"link":{"url":"(.*?)"}')

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
}


def get_data(url):
    file_path = 'someTxt'
    if not os.path.exists(file_path):
        os.mkdir(file_path)

    # 保存标题
    title_list = []
    # 保存图片链接
    img_link_list = []
    # 存放内容
    content_list = []
    # 具体内容链接
    content_url = []

    html = requests.get(url, headers=headers)
    soup = BeautifulSoup(html.text, 'lxml')
    titles = soup.select('.HotList-itemTitle')
    i = 1
    for title in titles:
        item = "标题 {}: {}".format(i, title.text)
        title_list.append(item + '\n')
        print(item)
        i += 1
    print('分割线'.center(50, '*'))

    imgs = soup.select('.HotList-itemImgContainer img')
    i = 1
    for img in imgs:
        item = "图片 {} 链接: {}".format(i, img['src'])
        img_link_list.append(item + '\n')
        # download_img(img['src'], i)
        print(item)
        i += 1
    print('分割线'.center(50, '*'))

    html_text = html.text
    contents = content_re.findall(html_text)
    # print(len(contents)) # 50
    i = 1
    for c in contents:
        item = "内容{}: {}".format(i, c)
        content_list.append(item + '\n\n')
        print(item)
        i += 1
    print('分割线'.center(50, '*'))

    urls = url_re.findall(html_text)
    i = 1
    for u in urls:
        u = u.replace("u002F", '').replace('\\', '/')
        item = '具体内容{}链接: {}'.format(i, u)
        content_url.append(item + '\n')
        print(item)
        i += 1

    # 知乎热版标题写到文件中
    with open(file_path + '/title_list.txt', 'w') as f:
        f.writelines(title_list)
    # 保存图片链接到文件中
    with open(file_path + '/img_link_list.txt', 'w') as f:
        f.writelines(img_link_list)
    # 存放内容和帖子链接
    with open(file_path + '/content.txt', 'w') as f:
        for i in range(len(content_list)):
            f.write(content_list[i].strip() + '\n')
            f.write(content_url[i] + '\n')


def download_img(img, num):
    file_path = 'zhihu_img'
    if not os.path.exists(file_path):
        os.mkdir(file_path)
    pic = requests.get(img, headers=headers)
    with open(file_path + '/' + str(num) + '.' + img.split('.')[-1], 'wb') as f:
        f.write(pic.content)


if __name__ == '__main__':
    url = 'https://www.zhihu.com/billboard'
    get_data(url)
