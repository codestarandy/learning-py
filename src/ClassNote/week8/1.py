import csv
import string

a = list(string.ascii_lowercase)
b = list(string.ascii_uppercase)
c = list(string.digits)
d = ['汉语', '数学', '英语', '物理']
l = [a, b, c, d]
# 注意一下newline参数
with open('test.csv', 'w', encoding='utf-8', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(l)
    # for i in l:
    #     writer.writerow(i)
