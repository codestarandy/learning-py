from random import randrange

grade = {'学号：20180' + str(i).zfill(2):
             {j: randrange(50, 150) for j in ['语文', '数学', '英语']}
         for i in range(1, 21)}
print(grade)

for item in grade.items():
    item[1]['总分'] = sum(item[1].values())
sum_score = sorted(grade.items(), key=lambda x: (-x[1]['总分'], x[0]))
print(sum_score)

title = '学号,' + ','.join(sum_score[0][1].keys())
print(title)

for item in sum_score:
    score = item[0][3:] + ',' + ','.join(map(str, item[1].values()))
    print(score)