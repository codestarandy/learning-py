"""
# 条形图
"""
import numpy as np
import matplotlib.pyplot as plt

"""
delimiter: 指定读取文件中数据的分割符，usecols: 指定需要读取的列，
unpack: 选择是否将数据进行向量输出， skiprows: 选择跳过的行数
"""
close_price = np.loadtxt('stock.csv', delimiter=',',
                         usecols=(4,), unpack=True, skiprows=1)

bins = np.arange(9, 14, 0.2)


plt.hist(close_price, bins, rwidth=0.8)  # 条形宽度设为80%
# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlabel('股票价格', fontsize=15)
plt.ylabel('出现次数', fontsize=15)
plt.title('收盘价分布直方图', fontsize=18)
plt.show()
