"""
饼图
"""
import matplotlib.pyplot as plt

labels = ['apple', 'pear', 'grape', 'waterma']
sizes = [15, 30, 45, 10]
explode = (0, 0.1, 0.1, 0)  # 只爆炸第二块饼，爆炸距离是半径的0.1。

# fig, ax1 = plt.subplots()
# ax1.pie(sizes, explode=explode, labels=labels, autopct='%.1f%%', pctdistance=0.7, shadow=True, startangle=90)
# ax1.axis('equal') # 等价于 ax1.set(aspect='euqal')，使得饼图在figure窗口放大缩小的过程中，保持圆形不变。
plt.pie(sizes, explode=explode, labels=labels, autopct='%.1f%%', pctdistance=0.7, shadow=True, startangle=45)

plt.show()
