# example11_3.py
# 案例：股票收盘价历史走势V2.0

# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

open_price, close_price = np.loadtxt('stock.csv', delimiter=',',
                                     usecols=(1, 4), unpack=True, skiprows=1)

x = np.arange(len(close_price))  # 横坐标值

plot1, = plt.plot(x, open_price, 'g--', linewidth=1)  # 绘制开盘价折线图
plot2, = plt.plot(x, close_price, 'r', linewidth=2)  # 绘制收盘价折线图

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']

plt.title('开盘价与收盘价历史走势图', fontsize=18)  # 添加图标题
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlabel('时间顺序', fontsize=15)  # 添加坐标轴标题
plt.ylabel('开盘价与收盘价', fontsize=15)
plt.xlim(0.0, max(x) + 1)  # 设定坐标轴限制
plt.ylim(min(min(open_price), min(close_price)) - 1,
         max(max(open_price), max(close_price)) + 1)
plt.legend((plot1, plot2), ('开盘价', '收盘价'),
           loc='lower right', fontsize=15, numpoints=1)  # 添加图例
plt.show()
