# example11_1.py
# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(10)  # 创建数组x
# np.random.seed(500)  # 随机数种子对后面的结果一直有影响
y = np.random.randint(0, 20, size=(10,))  # 创建数组y
# plt.plot(x, y,'-')          #绘制折线图
# plt.plot(x, y, 'r')
print(y)
plt.plot(x, y, 'r--', linewidth=3)
plt.show()  # 显示

