# %%
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(5)
y = x * 5
plt.plot(x, y)

plt.xticks(x, ('Tom', 'Dick', 'Harry', 'Sally', 'Sue'), rotation=90)
plt.yticks(y, ('A', 'B', 'C', 'D', 'E'), rotation=45)

plt.show()

# %%
plt.title('Interesting Graph', fontstyle='italic', backgroundcolor='y')
plt.plot([1, 2, 3, 4, 5], [3, 6, 7, 9, 2], 'r--')
plt.show()

# %%
x = np.arange(50)
y = x ** 5
plt.plot(x, y, 'b-')
plt.title('Interesting Graph', fontsize=22, fontweight='light', color='w', backgroundcolor='g')
plt.show()
