# 散点图
# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)
# np.random.rand(n) 返回n个0-1随机分布的浮点数
x = np.random.rand(10)
y = np.random.rand(10)

colors = np.random.rand(10)
area = (30 * np.random.rand(10)) ** 2

# alpha透明度
plt.scatter(x, y, s=area, c=colors, alpha=0.3, linewidths=50)
plt.show()
