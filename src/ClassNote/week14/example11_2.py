# example11_2.py
# 案例：股票收盘价历史走势V1.0
# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

close_price = np.loadtxt('stock.csv', delimiter=',', usecols=(4,),
                         unpack=True, skiprows=1)
x = np.arange(len(close_price))  # 横坐标值

plt.plot(x, close_price)  # 绘制折线图
# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('股票收盘价走势图', fontsize=18)  # 添加图标题
plt.xlabel('时间顺序', fontsize=15)  # 添加x轴标题
plt.ylabel('收盘价', fontsize=15)  # 添加y轴标题
plt.xlim(0.0, max(x) + 1)  # 设定x轴范围
# 设定y轴范围
plt.ylim(min(close_price) - 1, max(close_price) + 1)
plt.savefig('11_2.png')
plt.show()  # 显示
