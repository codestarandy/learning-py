# example11_6.py
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt

persons = [33, 65, 30, 30]
majors = ['信息管理与信息系统', '应用统计', '经济统计', '数据科学与大数据技术']
color = ['c', 'm', 'r', 'y']

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.figure(figsize=(7.5, 5))  # 设置图像大小
# startangle参数表示逆时针方向开始绘制的角度
# shadow表示是否显示阴影，explode表示突出显示某些切片
plt.pie(persons, labels=majors, colors=color, startangle=90, shadow=True,
        explode=(0.1, 0, 0, 0), autopct='%.1f%%',
        textprops={'fontsize': 15})

plt.title('各专业新生人数分布', fontsize=15, backgroundcolor='y')
plt.show()
