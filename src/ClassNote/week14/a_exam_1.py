import matplotlib.pyplot as plt

y = [4.38, 4.08, 4.11, 4.08]
x = list(range(len(y)))
all_terms = ['大一上', '大一下', '大二上', '大二下', '大三上', '大三下', '大四上', '大四下']
terms = all_terms[:len(y)]

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']

plt.plot(x, y)

plt.xticks(x, tuple(terms), rotation=90)
plt.title('绩点折线图', fontsize=22, fontweight='light', color='w', backgroundcolor='g')
plt.savefig('plt.png')
# plt.show()
