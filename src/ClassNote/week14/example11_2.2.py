# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 10, 1000)
y = np.sin(x)

plt.plot(x, y, 'b:')

plt.xlabel('x-axis')
plt.ylabel('y-axis')

# 设置x轴和y轴的取值范围
plt.xlim(-1, 12)
plt.ylim(ymin=-1.5, ymax=1.5)

plt.show()
