# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)
# 生成数据，以100组均值为70，方差为15的高斯分布数据为例
data = np.random.normal(70, 15, 100)
plt.hist(data, bins=100, color='g', edgecolor=None, histtype='bar', rwidth=0.5)
plt.show()

# %%
plt.hist(data, bins=50, color='b', edgecolor='r', histtype='step')
plt.show()
