# example11_9.py
# coding=utf-8
# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

winwide = 5  # 移动平均的窗口间隔

volume = np.loadtxt('stock.csv', delimiter=',',
                    usecols=(6,), unpack=True, skiprows=1)
print('Observation:\n', volume)
t = np.arange(winwide - 1, len(volume))
print('time:\n', t)

# 计算指数移动平均
weights = np.exp(np.linspace(-1, 0, winwide))  # 窗口内每期权重
weights /= weights.sum()
print('weights:\n', weights)
weightMovingAVG = np.convolve(weights, volume)  # 指数移动平均
print('Prediction:\n', weightMovingAVG)

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']
# 绘图
plot1 = plt.plot(t, volume[winwide - 1:], lw=1.0)  # 原始数据绘图
# 指数移动平均数据绘图
plot2 = plt.plot(t, weightMovingAVG[winwide - 1:1 - winwide], lw=3.0)

plt.title('交易量指数移动平均', fontsize=18)  # 添加图标题
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlabel('时间顺序', fontsize=15)  # 添加x坐标轴标题
plt.ylabel('交易量', fontsize=15)  # 添加y坐标轴标题
# 添加图例
plt.legend((plot1[0], plot2[0]), ('真实值', '指数移动平均值'),
           loc='upper right', fontsize=15, numpoints=1)
plt.show()
