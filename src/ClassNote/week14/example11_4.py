# example11_4.py
# 案例：股票收盘价历史分布V1.0 散点图
# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

close_price = np.loadtxt('stock.csv', delimiter=',',
                         usecols=(4,), unpack=True, skiprows=1)

x = np.arange(len(close_price))  # 横坐标值

colors = np.random.rand(40)
# plt.scatter(x,close_price,c='r',marker='o') #绘制收盘价散点图
plt.scatter(x, close_price, c=colors, marker='o', alpha=0.3, linewidths=20)  # 绘制收盘价散点图

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('收盘价历史分布图', fontsize=18)  # 添加图标题
plt.xlabel('时间顺序', fontsize=15)  # 添加坐标轴标题
plt.ylabel('收盘价', fontsize=15)

# 设定坐标轴限制
plt.xlim(0.0, max(x) + 1)
plt.ylim(min(close_price) - 1, max(close_price) + 1)
plt.show()
