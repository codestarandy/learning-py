import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 10, 1000)
y = np.sin(x)

plt.plot(x, y, 'b:')

# 添加 x 轴和 y 轴标题
plt.xlabel('x-axis')
plt.ylabel('y-axis')

plt.show()
