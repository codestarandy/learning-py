# example11_8.py
# coding=utf-8
# %%
import os
print(os.getcwd())

# %%
cur = os.getcwd().replace("\\", '/')
# 更改一下文件路径
os.chdir(cur + "/src/ClassNote/week14")
print(os.getcwd())

# %%
import numpy as np
import matplotlib.pyplot as plt

open_price, close_price = np.loadtxt('stock.csv', delimiter=',',
                                     usecols=(1, 4), unpack=True, skiprows=1)

winwide = 5  # 移动平均的窗口间隔
weights = np.ones(winwide) / winwide  # 窗口内每期数据的平均权重
# 开盘价简单移动平均
openMovingAvg = np.convolve(weights, open_price)
# 收盘价简单移动平均
closeMovingAvg = np.convolve(weights, close_price)
t = np.arange(winwide - 1, len(close_price))

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']

plt.figure(figsize=(18, 10))  # 设置图像大小
# 开盘价子图
plt.subplot(1, 2, 1)
plt.plot(t, open_price[winwide - 1:],
         lw=1.0, label='实际开盘价')
plt.plot(t, openMovingAvg[winwide - 1:1 - winwide],
         lw=3.0, label='开盘价移动平均值')
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid()
plt.title('开盘价', fontsize=18)
plt.legend(fontsize=15)

# 收盘价子图
plt.subplot(1, 2, 2)
plt.plot(t, close_price[winwide - 1:],
         lw=1.0, label='实际收盘价')
plt.plot(t, closeMovingAvg[winwide - 1:1 - winwide],
         lw=3.0, label='收盘价移动平均值')
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid()
plt.title('收盘价', fontsize=18)
plt.legend(fontsize=15)

# 调整子图间距
plt.subplots_adjust(wspace=0.2)
plt.show()
