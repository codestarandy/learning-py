# example11_7.py
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

open_price, high_price, low_price, close_price = np.loadtxt('stock.csv', delimiter=',',
                                                            usecols=(1, 2, 3, 4), unpack=True, skiprows=1)

x = np.linspace(0, len(open_price), 40)
plt.rcParams['font.sans-serif'] = ['SimHei']
# create figure
plt.figure(figsize=(12, 10))  # 设置图像大小
# create four axes
# first line, first column
ax1 = plt.subplot(2, 2, 1)
# first line, second column
ax2 = plt.subplot(2, 2, 2)
# second line, first column
ax3 = plt.subplot(2, 2, 3)
# second line, second column
ax4 = plt.subplot(2, 2, 4)

# choose ax1
plt.sca(ax1)
plt.plot(x, open_price, color='red')
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('开盘价', fontsize=15)

# choose ax2
plt.sca(ax2)
plt.plot(x, close_price, 'b--')
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('收盘价', fontsize=15)

# choose ax3
plt.sca(ax3)
plt.plot(x, high_price, 'g--')
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('最高价', fontsize=15)

# choose ax4
plt.sca(ax4)
plt.plot(x, low_price)
# 设置刻度字体大小
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('最低价', fontsize=15)

# 调整子图间距
plt.subplots_adjust(wspace=0.2, hspace=0.5)  # wspace左右间距，hspace上下间距
plt.show()
