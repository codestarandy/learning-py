# -*- coding: utf-8 -*-
# %%
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(5)
y = x * 5

# plt.plot([1, 2, 3, 4, 5], [3, 6, 7, 9, 2], 'r--')
# plt.plot(x, y, 'b-')
# plt.plot(x, y * 3, 'y:')
#
# plt.title('Interesting Graph')
# plt.show()

# 为了显示中文，指定默认字体
plt.rcParams['font.sans-serif'] = ['SimHei']

plot1, = plt.plot([1, 2, 3, 4, 5], [3, 6, 7, 9, 2], 'r--')
plot2, = plt.plot(x, y, 'b-')
plot3, = plt.plot(x, y * 3, 'y:')
# 设置legend
plt.legend((plot1, plot2, plot3), ('plot1', 'plot2', 'plot3'))

plt.title('有趣的图', fontsize=22, fontweight='light', color='w', backgroundcolor='g')
plt.show()
