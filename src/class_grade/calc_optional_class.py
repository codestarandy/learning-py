import csv


def read_csv(path):
    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        ls = list(reader)
    # print(ls)
    ls = [[j.strip() for j in i] for i in ls if len(i) > 0]
    print(ls)
    credit = []
    for i in ls:
        credit.append(i[1])
    credit = [float(i) for i in credit]
    print('已经修过的学分数: ', sum(credit))
    print(credit)



if __name__ == '__main__':
    read_csv('files/optional_course.csv')
