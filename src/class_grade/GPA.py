import csv


def cal_gpa(path, precision=4):
    """
    计算您相对路径为path的文件里面的科目的绩点
    :param path: 文件的相对路径, 文件格式是一个csv, 每一行是 科目,学分,成绩 , 其中成绩是百分制
    :param precision: 最后绩点保留的精度
    :return: GPA
    """
    with open(path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        ls = list(reader)
    ls = [[j.strip() for j in i] for i in ls if len(i) > 0]
    print('总共修了 {} 门课'.format(len(ls)))

    for i in ls:
        # 判断条件是因为我记得不及格不算成绩
        single = (eval(i[2]) - 50) / 10 if eval(i[2]) > 60 else 0
        i.append(single)

    print('各科目名称, 学分, 成绩, 如下: \n', ls)

    credit, score = 0, 0
    # 计算绩点
    for i in ls:
        if i[3] > 0:
            score += eval(i[1]) * i[3]
            credit += eval(i[1])

    # score是单科绩点乘以学分, score是总学分
    # print(score)
    # print(credit)

    GPA = round(score / credit, precision)
    print('这 {} 个科目的总绩点为 {} -> {}'.format(len(ls), GPA, round(GPA, 2)))
    return GPA


if __name__ == '__main__':
    cal_gpa('files/2_2_grade.csv', 4)
