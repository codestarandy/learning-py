f = open('files/aa.txt', 'r')
word_freq = {}
for line in f:
    words = line.strip().replace(',', ' ').replace('.', ' ').split()
    for word in words:
        word_freq[word] = word_freq.get(word, 0) + 1
f.close()
freq_word = []
for word, freq in word_freq.items():
    freq_word.append((freq, word))
freq_word.sort(reverse=True)
for i in range(10):
    print(freq_word[i])