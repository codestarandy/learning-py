s = 'middle- of-the-road'
word_len = len(s)
if '-' in s:
    # 连字符个数
    num_hyphen = 0
    for c in s:
        if '-' == c:
            num_hyphen += 1
    word_len -= num_hyphen
if ' ' in s:
    # 空格个数
    num_blank = 0
    for c in s:
        if ' ' == c:
            num_blank += 1
    word_len -= num_blank
print(word_len)
