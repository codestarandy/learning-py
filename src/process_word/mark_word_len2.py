def calc_word_len(directory, num):
    """

    :param directory: 文件夹
    :param num: 第num篇课文
    :return: 长度列表
    """
    with open(directory + str(num) + '.txt', 'r') as f:
        words = f.readlines()
    words = [s.replace('\n', '').strip() for s in words]
    words = [s for s in words if len(s) > 0]
    words_len = [len(w) for w in words]
    for item in range(len(words)):
        # print(words[item])
        # 中间有连字符应该len要减去连字符个数
        words_len[item] -= words[item].count('-')
        # 中间有空格应该len要减去空格个数
        words_len[item] -= words[item].count(' ')
        print('单词 {:^18} 的长度是{:^5}个字母'.format(words[item], words_len[item]))


if __name__ == '__main__':
    i = 9   # 第几篇课文
    print('lesson {} 的单词'.format(i).center(40, '*'))
    calc_word_len('In_class_words/', i)
